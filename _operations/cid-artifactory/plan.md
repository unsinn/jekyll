---
layout: page
title:  "CID-Artifactory integration"
categories: cid-artifactory
---

h1. CID / Artifactory integration, v4.0 ( using builds )

h2. CID client operations
#  *promote* - creates an alias ( symlink ) to a artifact on archive server
#  *version*   - shows available aliases
#  *pin*  - pin specific  alias to a specific CID project, environment 
#  *deploy*  is carried out in two ways - deploy pinned artifact or last successful one.

h2. CID client operations mapping to artifactory

h3.  Prerequisites 

# Artifactory
 #* Artifacts are being deployed to artifactory by jenkins with jenkins/artifactory plugin in generic artifact  _Generic-Artifactory Integration_ flavour 
 #* Artifacts deployed as builds ( which is by default with jenkins / artifactory plugin )
 #* Build name should be equal to CID project name ( which means jenkins job name should be equal to CID project name )
# CID
 #* Every CID project  must have a *components list* - the list of artifacts to deploy
   #*  every item in the components list is a _component_ represents artifact to deploy,  having following attributes:
   #** *filename* - file name of artifact
   #** *id* - a component ID, string
 #* Component list  example 
      #**   id - main
      #***  filename  - guess-a-number.war
      #**   id - test
      #***  filename  - test.war
 #* Every \[CID project,deploy environment\]  must have a build_name property relates to builds from artifactory
    #** Example: CID project - guess, deploy environment - test, build_name - guess-test


h3. Operations 

 #  *promote*
  #* Instead of calling this action `promote' I'd like to use term `aliasing' or `tagging' for this
  #* Implemented by - sets short description to the build
  #* Example. Tag build with name 'RC' for dev/prod deploy enviromnets.  CID project - guess, deploy environments -test, production, build_name - guess, build.number - 50 
{code}
POST  api/build/promote/guess/50
{
  properties: {
     "cid.alias": ["RC"],
     "cid.env": ["test", "production"]   
  }
}
{code}
  #* The list of _aliased_ builds may be retrieved by property search
  #* Example. Find all aliased builds for CID project - guess, deploy environment - test
{code}
    GET api/search/prop/cid.alias&build.name=guess&cid.env=test -H 'X-Result-Detail: properties'
{code}
 #* The return results should be grouped by build.number, finaly we have a build with build numbers and properties 
#  *pin* 
 #* Bind a specific build to  \[CID project, deploy environment\] 
 #* Executed by setting build propery
 #* Example - pin build with number 50 to `dev' and `test' environments, CID project guess
{code}
POST  api/build/promote/guess/50
{
  properties: {
     "cid.pin": ["dev","test"]
  }
}
{code}
 #* _Pinned_ builds may by retrieved this property search query:
{code}
    GET api/search/prop/cid.alias&build.name=guess&cid.pin=test 
{code}
 #* The return results should be grouped by build number 
 #* Finally we have a single build containing all artiacts to deploy
 #* In case there are more then one builds found an exception should be raised
 #* Build artifacts are retrieved by this query 
{code}
POST /api/search/buildArtifacts
{
    "buildName" : "guess",
    "buildNumber" : "50"
}
{code}
# *Deploy*
 #* *automatic deploys* 
  #** are carried out in two ways 
  #*** if \[CID project, deploy environment\] has a pinned build - deploy artifacts for this build
  #***  otherwise _latest build_  used
  #*** latest build is retrieved by this query
{code}
POST /api/search/buildArtifacts
{
    "buildName" : "guess",
    "buildNumber" : "LATEST"
}
{code}
#* *manual deploy* 
 #** When user initiates deploy via webUI he can choose artifact to deploy from versions list (  see `version' operation description  ) 
 #** If user does not choose artifacts from versions list, pinned \ latest build gets deployed
# Special use cases
#* Renaming jenkins job. In case jenkins job is renamed one should rename related build and update build_name property for CID project, deploy environment.
    #*** Example
{code}
    POST api/build/rename/guess?to=guess-new
{c
